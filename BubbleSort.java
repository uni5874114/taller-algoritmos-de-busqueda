package bubblesort;

import java.util.Random;


public class BubbleSort {

    public static void main(String[] args) {
        Random ran = new Random();
        int tamano = 2000;
        int [] arr = new int [tamano];
        for (int i = 0; i < tamano; i++) {
            arr[i] = ran.nextInt(3000);
        }
        System.out.println("ARRAY ORIGINAL: ");
        for(int value : arr){
            System.out.print(value + " ");
        }
        
        //algoritmo:
        long inicio = System.currentTimeMillis();
        for (int i = 0; i < arr.length ; i++) {
            for (int j = 0; j < arr.length-i-1; j++) {
                if (arr[j]>arr[j+1]) {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1]= temp;   
                }   
            }
        }
        System.out.println();
        System.out.println("ARRAY with Bubble Sort: ");
        for(int elem :arr){
            System.out.print(elem + " ");
        }
        System.out.println();
        long fin = System.currentTimeMillis();
                
                long tiempoTotal = fin - inicio;
                
        System.out.println("Tiempo total de ejecucion: " + tiempoTotal +" Milisegundos");
        
        
        
       
        
        
    }// CIERRA MAIN
    
}// CIERRA CLASS