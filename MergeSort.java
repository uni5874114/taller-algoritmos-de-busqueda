package mergesort;

import java.util.Random;

public class MergeSort {

    public static void main(String[] args) {
        MergeSortClass mergeSort = new MergeSortClass();

        Random ran = new Random();
        int tamano =2000;
        int []arr = new int [tamano];
        
         for (int i = 0; i < tamano; i++) {
            arr[i] = ran.nextInt(3000); 
        }
        
        
        System.out.println("ARRAY ORIGINAL: ");
         long startTime = System.currentTimeMillis(); // Tiempo de inicio
        for(int value : arr){
            System.out.print(value + " ");
        }
        System.out.println();
        
        System.out.println("ARRAY ORDENADO: ");
        mergeSort.sort(arr, 0, tamano-1);
        mergeSort.printArray(arr);
        long endTime = System.currentTimeMillis(); // Tiempo de finalización
        long tiempoEjecucion = endTime - startTime;
        
        System.out.println("\nTiempo de ejecución: " + tiempoEjecucion + " Milisegundos");
    }
    
}
//
package mergesort;

public class MergeSortClass {
    
    public void sort(int arr[], int left, int right){
        if(left<right){
            int middle = (left + right)/2;
            
            sort(arr, left,middle);
            sort(arr, middle+1,right);
            
            merge(arr,left,middle,right);
         
        }
    }//dividirEnDosMitadesElVector
    
    public void merge(int arr[],int left, int middle, int right){
        int n1 = middle - left +1;
        int n2 = right - middle;
        
        int leftArray[] = new int [n1];
        int rightArray[] = new int [n2];
        
        for (int i = 0; i< n1 ; i++){
            leftArray[i] = arr[left+i];
        }
        for (int j = 0; j<n2 ; j++){
            rightArray[j] = arr[middle + j + 1];
        }
        
        int i =0, j =0;
        
        int k = left;
        
        while(i < n1 && j < n2){
            if(leftArray[i]<= rightArray[j]){
                arr[k] = leftArray[i];
                i++;
            }else{
                arr[k] = rightArray[j];
                j++;
            }
            k++;
        }
        while(i < n1){
            arr[k] = leftArray[i];
            i++;
            k++;
        }
        
        while(j < n2){
            arr[k] = rightArray[j];
            j++;
            k++;
        }
    }//unirLosSubVectores
    
    public void printArray(int arr[]){
        int n = arr.length;
        for(int i = 0; i < n ; i++){
            System.out.print(arr[i] + " ");
        
        }
        System.out.println();
    } // ImprimirArreglo
