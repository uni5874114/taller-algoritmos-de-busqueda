package quicksort;

import java.util.Random;

public class QuickSort {

    public static void main(String[] args) {

        Random ran = new Random();
        int tamano = 2000;
        int[] vec = new int[tamano];

        for (int i = 0; i < tamano; i++) {
            vec[i] = ran.nextInt(2500);
        }

        System.out.println("Vector original");

        imprimirVector(vec);
        long startTime = System.currentTimeMillis(); // Tiempo de inicio

        ordenacionRapida(vec);
        long endTime = System.currentTimeMillis(); // Tiempo de finalización
        long tiempoEjecucion = endTime - startTime;

        System.out.println("\nVector ordenado");
        imprimirVector(vec);

       System.out.println("\nTiempo de ejecución: " + tiempoEjecucion + " Milisegundos");
    }

    public static void ordenacionRapida(int vec[]) {

        final int N = vec.length;
        quickSort(vec, 0, N - 1);

    }

    public static void quickSort(int vec[], int inicio, int fin) {

        if (inicio >= fin) {
            return;
        }
        int pivote = vec[inicio];
        int elemIzq = inicio + 1;
        int elemDer = fin;
        while (elemIzq <= elemDer) {
            while (elemIzq <= fin && vec[elemIzq] < pivote) {
                elemIzq++;
            }
            while (elemDer > inicio && vec[elemDer] >= pivote) {
                elemDer--;
            }
            if (elemIzq < elemDer) {
                int temp = vec[elemIzq];
                vec[elemIzq] = vec[elemDer];
                vec[elemDer] = temp;
            }
        }

        if (elemDer > inicio) {
            int temp = vec[inicio];
            vec[inicio] = vec[elemDer];
            vec[elemDer] = temp;
        }

        quickSort(vec, inicio, elemDer - 1);
        quickSort(vec, elemDer + 1, fin);

    }

    public static void imprimirVector(int vec[]) {
        for (int i = 0; i < vec.length; i++) {
            System.out.print(vec[i] + " ");
        }

    }

}
